﻿namespace Base2art.SimpleBuilder.CodeCheckout.Git
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using LibGit2Sharp;

    public class GitCheckoutSystem : CodeCheckoutSystemBase<GitData>
    {
        public GitCheckoutSystem()
        {
        }
        
        public override string Type
        {
            get { return "GIT"; }
        }

        protected override Task Checkout(GitData settings, string destinationPath)
        {
            return Task.Factory.StartNew(() => this.CheckoutSync(settings, destinationPath));
        }

        private void CheckoutSync(GitData settings, string destinationPath)
        {
            var options = new CloneOptions();
            if (!string.IsNullOrWhiteSpace(settings.UserName) && !string.IsNullOrWhiteSpace(settings.Password))
            {
                options.CredentialsProvider = (_url, _user, _cred) => new UsernamePasswordCredentials {
                    Username = settings.UserName,
                    Password = settings.Password
                };
            }
            
//            if (!string.IsNullOrWhiteSpace(settings.KeyName))
//            {
//                options.CredentialsProvider = (_url, _user, _cred) => this.Process(_url, _user, _cred, settings);
//            }
            
            Repository.Clone(settings.CloneLocation, destinationPath, options);
        }
        
//        private Credentials Process(string _url, string _user, SupportedCredentialTypes _cred, GitData settings)
//        {
//            var cred = new SshUserKeyCredentials {
//                Username = _user,
//                Passphrase = string.Empty,
//                PublicKey = Path.Combine(this.keysDir, settings.KeyName + ".pub"),
//                PrivateKey = Path.Combine(this.keysDir, settings.KeyName),
//            };
//            
//            return cred;
//        }
    }
}