﻿namespace Base2art.SimpleBuilder
{
	using System;

    public class DeploymentData
    {
        public DateTime When { get; set; }
        
        public string User { get; set; }
        public string SiteName { get; set; }
        public string Environment { get; set; }
        public string Version { get; set; }
    }
}


