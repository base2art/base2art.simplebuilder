﻿namespace Base2art.SimpleBuilder.Public.Tasks
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Models;
    
    public class ProvisionMasterDatabaseTask
    {
        private readonly IDataStore store;

        private readonly IDbms dbms;

        public ProvisionMasterDatabaseTask(IDbmsFactory dbmsFactory, IDataStoreFactory store)
        {
            this.dbms = dbmsFactory.Create("build-master");
            this.store = store.Create("build-master");
        }
        
        public async Task ExecuteAsync()
        {
            await this.CreateTable(() => this.dbms.CreateTable<build>());
            await this.CreateTable(() => this.dbms.CreateTable<build_server>());
            await this.CreateTable(() => this.dbms.CreateTable<project>());
        }

        private async Task CreateTable<T>(Func<ITableCreator<T>> par)
        {
            try
            {
                await par().Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }
        }
    }
}
