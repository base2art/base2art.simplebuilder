﻿namespace Base2art.SimpleBuilder.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.SimpleBuilder.dbo;
    using Base2art.SimpleBuilder.Public.Resources;
    
    public class CleanBuildsTask
    {
        private readonly IDataStore store;

        private readonly Guid machine_id;

        private readonly DirectoryInfo workingDir;
        
        public CleanBuildsTask(
            IDataStoreFactory store,
            Guid machineId,
            string baseWorkingDir)
        {
            this.machine_id = machineId;
            this.store = store.Create("builds");
            
            this.workingDir = Directory.CreateDirectory(baseWorkingDir);
        }
        
        public async Task ExecuteAsync()
        {
            Guid g = Guid.NewGuid();
            
            var ids = await this.store.Select<node_build>()
                .Fields(rs => rs.Field(x => x.id))
                .WithNoLock()
                .Where(rs => rs.Field(x => x.machine_id, machine_id, (x, y) => x == y)
                       .FieldIn(x => x.state, new int[] {
                                    (int)BuildState.Claimed,
                                    (int)BuildState.Pending,
                                    (int)BuildState.Working
                                }))
                .Execute();
            
            var idSet = new HashSet<string>(ids.Select(x => x.id.ToString("N")), StringComparer.OrdinalIgnoreCase);
            
            foreach (var folder in this.workingDir.EnumerateDirectories())
            {
                if (folder.Name.Length == Guid.Empty.ToString("N").Length && !idSet.Contains(folder.Name))
                {
                    TryDelete(folder, 0, 3);
                }
            }
        }

        private static void TryDelete(DirectoryInfo checkoutLocation, int current, int max)
        {
            try
            {
                if (checkoutLocation.Exists)
                {
                    checkoutLocation.Delete(true);
                }
            }
            catch (Exception)
            {
                if (current >= max)
                {
                    throw;
                }
                
                TryDelete(checkoutLocation, current + 1, max);
            }
        }
    }
}
