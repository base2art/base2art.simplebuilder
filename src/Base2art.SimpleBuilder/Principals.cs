﻿namespace Base2art.SimpleBuilder
{
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using Base2art.WebApiRunner;
    using RestSharp;
    public static class Principals
    {
        

        public static void RequireSignedIn(this IPrincipal principal)
        {
            if (principal == null || !principal.Identity.IsAuthenticated)
            {
                throw new NotAuthenticatedException();
            }
        }

        public static string Token(this ClaimsPrincipal principal)
        {
            var result = principal.Claims.First(x => x.Type == "token").Value;
            return result;
        }
        
        public static void Prep(this RestRequest request, ClaimsPrincipal principal)
        {
            request.RequestFormat = DataFormat.Json;
            request.OnBeforeDeserialization = resp =>
            {
                resp.ContentType = "application/json";
            };
            request.JsonSerializer = new JsonSerializer();
            request.AddHeader("Authorization", "Bearer " + principal.Token());
            request.AddHeader("Accept", "application/json");
        }
    }
}
