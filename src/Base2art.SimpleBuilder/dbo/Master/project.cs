﻿namespace Base2art.SimpleBuilder.dbo
{
    using System;
    using System.Collections.Generic;

    public interface project
    {
        Guid id { get; }
        
        string name { get; }
        
        string power_shell_script { get; }
        
        string source_control_type { get; }
        
        Dictionary<string, object> source_control_data { get; }
        
        string list_of_features { get; }
    }
    
    
    
}
