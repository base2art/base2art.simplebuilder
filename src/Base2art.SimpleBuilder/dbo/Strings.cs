﻿

namespace Base2art.SimpleBuilder.dbo
{
    using System;
    using System.Collections.Generic;
    
    public static class Strings
    {
        public static string[] Split(this string features)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(features ?? "[]");
        }
        
        public static string Join(this string[] features)
        {
            features = features ?? new string[0];
            return Newtonsoft.Json.JsonConvert.SerializeObject(features);
        }

        public static Dictionary<string, object> ObjectToDict(this object data)
        {
            var content = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(content);
        }
    }
}
