﻿
namespace Base2art.SimpleBuilder
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using RestSharp;
    using RestSharp.Deserializers;
    using RestSharp.Serializers;

    public class JsonDeserializer : IDeserializer
    {
        public string RootElement
        {
            get;
            set;
        }

        public string Namespace
        {
            get;
            set;
        }

        public string DateFormat
        {
            get;
            set;
        }

        public CultureInfo Culture
        {
            get;
            set;
        }

        public JsonDeserializer()
        {
            Culture = CultureInfo.InvariantCulture;
        }


//        T Deserialize<T>(IRestResponse response);
        public T Deserialize<T>(IRestResponse response)// where T : new()
        {
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}


