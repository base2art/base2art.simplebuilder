﻿namespace Base2art.SimpleBuilder.CodeCheckout
{
    using System.Collections.Generic;
    using System.Linq;
    
    public static class ObjectExtensions
    {
        public static void CopyTo<T>(this IDictionary<string, object> source, T destination)
        {
            var someObjectType = typeof(T);
            var properties = someObjectType.GetProperties();
            
            foreach (var item in source)
            {
                var prop = properties.FirstOrDefault(p => string.Equals(p.Name, item.Key, System.StringComparison.OrdinalIgnoreCase));
                
                if (prop != null)
                {
                    prop.SetValue(destination, item.Value, null);
                }
            }
        }

        //        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        //        {
        //            return source.GetType().GetProperties(bindingAttr).ToDictionary(propInfo => propInfo.Name, propInfo => propInfo.GetValue(source, null));
        //        }
    }
}


